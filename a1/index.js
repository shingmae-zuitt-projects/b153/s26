let http = require("http")

const server = http.createServer(function(request, response){

	if(request.url === '/api/users' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("Users retrieved from database.")

	}else if(request.url === '/api/users/101' && request.method === "GET"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("user 123 retrieved from database.")

	}else if(request.url === '/api/users/101' && request.method === "PUT"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("user 123 updated.")	

	}else if(request.url === '/api/users/101' && request.method === "DELETE"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("user 123 deleted.")	
			
	}else if(request.url === '/api/users' && request.method === "POST"){
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end("New user added to database.")
				
	}
})

const port = 5000

server.listen(port)

console.log(`Server running at port ${port}`)